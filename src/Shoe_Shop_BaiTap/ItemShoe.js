import React, { Component } from 'react';
import { connect } from 'react-redux';
import { chonMuaAction } from './redux/actions/chonMuaAction';
import { xemChiTietAction } from './redux/actions/xemChiTietAction';

class ItemShoe extends Component {
  render() {
    let { name, image, price, description } = this.props.data;
    return (
      <div className="col-3 my-4 text-center">
        <div className="card h-100">
          <img className="card-img-top" src={image} alt="Card image cap" />
          <div className="card-body">
            <h5 className="card-title">{name}</h5>
            <p className="card-text">{description}</p>
            <h3 className="my-3">{price}$</h3>
          </div>
          <button
            onClick={() => {
              this.props.handleXemChiTiet(this.props.data);
            }}
            className="btn mb-2  btn-info"
          >
            XEM CHI TIẾT
          </button>
          <butto
            onClick={() => {
              this.props.handleChonMua(this.props.data);
            }}
            className="btn btn-success"
          >
            CHỌN MUA
          </butto>
        </div>
      </div>
    );
  }
}

let mapDispatchToProps = (dispatch) => {
  return {
    handleXemChiTiet: (shoe) => {
      dispatch(xemChiTietAction(shoe));
    },
    handleChonMua: (shoe) => {
      dispatch(chonMuaAction(shoe));
    },
  };
};
export default connect(null, mapDispatchToProps)(ItemShoe);
