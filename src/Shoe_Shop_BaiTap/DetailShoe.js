import React, { Component } from 'react';
import { connect } from 'react-redux';
class DetailShoe extends Component {
  render() {
    let { image, name, price, description } = this.props.detailShoe;
    console.log(this.props.detailShoe);
    return (
      <div className="bg-info row">
        <img className="col-4" src={image} alt="" />
        <div className="col-8 d-flex flex-column justify-content-center align-items-center ">
          <h3>{name}</h3>
          <p>{description}</p>
          <h4>{price}$</h4>
        </div>
      </div>
    );
  }
}

// LAY DU LIEU
let mapStateToProps = (state) => {
  return {
    detailShoe: state.detailShoe,
  };
};

export default connect(mapStateToProps)(DetailShoe);
