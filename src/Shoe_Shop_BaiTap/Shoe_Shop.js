import React, { Component } from 'react';
import Cart from './Cart';
import DetailShoe from './DetailShoe';
import ListShoe from './ListShoe';

export default class Shoe_Shop extends Component {
  render() {
    return (
      <div className="container">
        <DetailShoe />
        <ListShoe />
        <Cart />
      </div>
    );
  }
}
