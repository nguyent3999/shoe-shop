import React, { Component } from 'react';
import { connect } from 'react-redux';
import ItemShoe from './ItemShoe';

class ListShoe extends Component {
  renderListShoe = () => {
    return this.props.dataShoe.map((item, index) => {
      return <ItemShoe data={item} key={index} />;
    });
  };
  render() {
    return <div className="row">{this.renderListShoe()}</div>;
  }
}

// LAY DU LIEU TU STATE
let mapStateToProps = (state) => {
  return {
    dataShoe: state.dataShoe,
  };
};

export default connect(mapStateToProps)(ListShoe);
