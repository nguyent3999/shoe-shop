import { dataShoe } from '../../dataShoe';
import {
  CHON_MUA,
  TANG_GIAM_SO_LUONG,
  XEM_CHI_TIET,
} from '../constants/shoeShopConstants';

let initialState = {
  dataShoe: dataShoe,
  detailShoe: dataShoe[0],
  cart: [],
};
export const shoeShopReducer = (state = initialState, action) => {
  switch (action.type) {
    case XEM_CHI_TIET:
      return { ...state, detailShoe: action.payload };
    case CHON_MUA: {
      let cloneCart = [...state.cart];
      let index = cloneCart.findIndex((item) => {
        return item.id == action.payload.id;
      });
      if (index == -1) {
        let cartItem = { ...action.payload, soLuong: 1 };
        cloneCart.push(cartItem);
      } else cloneCart[index].soLuong++;
      return { ...state, cart: cloneCart };
    }
    case TANG_GIAM_SO_LUONG: {
      let cloneCart = [...state.cart];
      let index = cloneCart.findIndex((item) => {
        return item.id == action.payload.shoeId;
      });
      cloneCart[index].soLuong += action.payload.number;
      cloneCart[index].soLuong < 1 && cloneCart.splice(index, 1);
      return { ...state, cart: cloneCart };
    }

    default:
      return state;
  }
};
