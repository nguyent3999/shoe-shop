import { CHON_MUA } from '../constants/shoeShopConstants';

export const chonMuaAction = (shoe) => {
  return {
    type: CHON_MUA,
    payload: shoe,
  };
};
