import { XEM_CHI_TIET } from '../constants/shoeShopConstants';

export const xemChiTietAction = (value) => {
  return {
    type: XEM_CHI_TIET,
    payload: value,
  };
};
