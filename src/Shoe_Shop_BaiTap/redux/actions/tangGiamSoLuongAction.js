import { TANG_GIAM_SO_LUONG } from '../constants/shoeShopConstants';

export const tangGiamSoLuongAction = (shoeId, number) => {
  return {
    type: TANG_GIAM_SO_LUONG,
    payload: {
      shoeId,
      number,
    },
  };
};
