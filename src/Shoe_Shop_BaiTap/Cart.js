import React, { Component } from 'react';
import { connect } from 'react-redux';
import { tangGiamSoLuongAction } from './redux/actions/tangGiamSoLuongAction';
class Cart extends Component {
  renderTbody = () => {
    return this.props.cart?.map((item) => {
      let { name, image, price, soLuong, id } = item;
      return (
        <tr>
          <td>{name}</td>
          <td>
            <img style={{ width: '100px' }} src={image} alt="" />{' '}
          </td>
          <td>{price * soLuong}</td>
          <td>
            <button
              onClick={() => {
                this.props.handleTangGiamSoLuong(id, -1);
              }}
              className="btn btn-danger"
            >
              -
            </button>
            <span className="mx-2">{soLuong}</span>
            <button
              onClick={() => {
                this.props.handleTangGiamSoLuong(id, +1);
              }}
              className="btn btn-success"
            >
              +
            </button>
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <table className="table">
        <thead>
          <th>Name</th>
          <th>Image</th>
          <th>Price</th>
          <th>Quantity</th>
        </thead>
        <tbody>{this.renderTbody()}</tbody>
      </table>
    );
  }
}

// lay du lieu
let mapStateToProps = (state) => {
  return {
    cart: state.cart,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    handleTangGiamSoLuong: (shoeId, index) => {
      dispatch(tangGiamSoLuongAction(shoeId, index));
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(Cart);
